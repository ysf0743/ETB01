// pages/myinfo/myinfo.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    lists: [
      {
        title: '我的地址',
        icon: '/img/myinfo/dz.svg'
      },
      {
       
      
        title: '我要送餐',
        iScon: '/img/myinfo/kf.svg'
      },
      {
        title: '帮助和反馈',
        icon: '/img/myinfo/help.svg'
      },
      {
        title: '协议和说明',
        icon: '/img/myinfo/sm.svg'
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let that = this;
    wx.setNavigationBarTitle({
      title: '个人中心',
    });
    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#f8c724',
    })
  },
  onTouch1: function(options){
    wx.navigateTo({
      url: '../logs/logs',
      
    })
  },
  onTouch2: function (options) {
    wx.navigateTo({
      url: '../address/address',

    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})