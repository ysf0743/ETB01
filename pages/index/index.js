//获取应用实例
const app = getApp()

Page({

  data: {
    imgUrls: [
      'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1566737698705&di=6cfdc3cbd36117b7e7de3337f0d00f74&imgtype=0&src=http%3A%2F%2Fimg.zcool.cn%2Fcommunity%2F01046758ddc7eaa801219c779a734f.jpg%402o.jpg',
      'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1566737775709&di=79d112958df76d8b0275b302a793c69a&imgtype=0&src=http%3A%2F%2Fimg3.redocn.com%2Ftupian%2F20150112%2Fdoujiaoshaoqiezi_3531232.jpg',
      'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1566737976530&di=69c27c4e40be2e3d9c3adf024e9debbb&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201712%2F01%2F20171201215745_5zcEi.thumb.700_0.jpeg',
      'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1566738107254&di=5a053fabfdfc744ddf6aa0afcdb1bbc8&imgtype=0&src=http%3A%2F%2Fs1.sinaimg.cn%2Flarge%2F0028C0tngy6Lzvtf0OI00'
    ],

    storesInfo: {},
    sortChoosed: '1' //判断排序方式
     },
     
  onLoad: function () {
    this.setData({
      storesInfo: wx.getStorageSync('storesInfo')
    })
  },
  // 排序方式不同,1-综合排序，2-销量最高，3-速度最快
  sortChoose(event) {
    let sortChoosed = event.currentTarget.dataset.type;
    let storesInfo = this.data.storesInfo;
    switch (sortChoosed) {
      case "1":
        storesInfo = wx.getStorageSync('storesInfo');
        break;
      case "2":
      case "3":
        storesInfo = this.changeCopy(sortChoosed);
        break;
      default:
        break;
    }
    this.setData({
      sortChoosed,
      storesInfo
    })
  },
  //销量，速度排序
  changeCopy(types) {
    let storesInfo = this.data.storesInfo;
    // 冒泡排序
    for (let i = 0; i < storesInfo.length; i++) {
      for (let j = 0; j < storesInfo.length - i - 1; j++) {
        // 如果是2，按销售排序
        if (types == "2") {
          if (storesInfo[j + 1].sale > storesInfo[j].sale) {
            let temp = JSON.parse(JSON.stringify(storesInfo[j]));
            storesInfo[j] = JSON.parse(JSON.stringify(storesInfo[j + 1]));
            storesInfo[j + 1] = JSON.parse(JSON.stringify(temp));
          }
        }
        // 如果是3，按速度排序
        if (types == "3") {
          if (storesInfo[j].time > storesInfo[j + 1].time) {
            let temp = JSON.parse(JSON.stringify(storesInfo[j]));
            storesInfo[j] = JSON.parse(JSON.stringify(storesInfo[j + 1]));
            storesInfo[j + 1] = JSON.parse(JSON.stringify(temp));
          }
        }
      }
    }
    return storesInfo;
  }

})