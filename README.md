今天我们组添加了我的订单页面，订单提交页面，我的信息页面。
其中我负责订单提交页面。代码如下：
// pages/payOrders/payOrders.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    payOrders: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      payOrders: wx.getStorageSync('payOrders')
    })
    wx.setNavigationBarTitle({
      title: '提交订单',
    });
    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#f4f4f4',
    })
  },
})


<!--pages/payOrders/payOrders.wxml-->
<view class='container'>

  <!-- 头部外卖配送 -->
  <view class='topbar' id="{{item.sid}}" url="/pages/detail/detail?sid={{item.sid}}">
    <view class='top_type'>
      <view class='dispatch'>外卖配送</view>
      <view class='self'>到店自取</view>
    </view>
    <view class='position'>
      <view class='top_img'>
        <image src="/img/position.svg"></image>
      </view>
      <view class='userinfo'>
        
        <view class="section">
  <input placeholder="输入地址" auto-focus/>
</view>
        
      </view>
    </view>
    <view class='time'>
      <view class='top_img'>
        <image src="/img/time.svg"></image>
      </view>
      <view class='time_info'>送达时间 及时送达</view>
    </view>
  </view>
  <!-- 中间菜品列表-->
  <view class="middle">
    <!-- 商家信息 -->
    <view class='stores'>
      <view class='s_img'>
        <image src="{{payOrders.currentSto.img}}"></image>
      </view>
      <view class='s_name'>{{payOrders.currentSto.name}} </view>
    </view>
    <!-- 订单信息 -->
    <view class='detail_lists' wx:for="{{payOrders.orderList}}" wx:key="{{index}}">
      <view class='list_img'>
        <image src="{{item.img}}"></image>
      </view>
      <view class='list_info'>
        <view class='list_name'>{{item.name}}</view>
        <view class='list_num' style='color:#ccc'>x{{item.num}}</view>
      </view>

      <view class='list_cost'>￥{{item.cost}}</view>
    </view>
    <view class='total'>
      小计
      <text style='color:red;font-weight:600;'>￥{{payOrders.totalcost}}</text>
    </view>
  </view>
  <!--支付方式  -->
  <view class='otherinfo'>
    <view class='payways'>支付方式</view>
    <view>线下支付</view>
  </view>
</view>

<!-- 微信支付 -->

  <view class='chunk color1'>订单成功</view>
  <view class='chunk color2'>联系我们</view>